<?php

/**
 * Created by Jeff pedro.
 * Date: 28/10/2014
 * Time: 12:05
 */
class Email
{
    private $url = "https://mandrillapp.com/api/1.0/messages/send-template.json";

    public function sendEmail($recipient, $data, $layout, $attachments = null)
    {
        if ($recipient['email'] !== "" && $layout !== "") {

            $data = '{
                    "key": "your key",
                    "template_name": "' . $layout . '",
                    "template_content": [{
                        "name":"default",
                        "content":"default"
                    }],
                    "message": {
                        "to": [{
                            "email": "' . $recipient['email'] . '",
                            "name": "' . $recipient['name'] . '"
                        }],
                        "global_merge_vars": [' . $this->getData($data) . ']
                    }
                }';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->url);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            $result = json_decode(curl_exec($ch), true);

            if ($result[0]['status'] == 'sent')
                return true;
            elseif ($result[0]['status'] == 'rejected')
                return $result[0]['reject_reason'];
            else
                return $result[0]['reject_reason'];

        } else {
            return 'Email and layout is not set';
        }
    }

    private function getData($data)
    {
        $global_vars = "";
        foreach ($data as $name => $content) {
            if ($content !== "")
                $global_vars .= '{
                                "name": "' . $name . '",
                                "content": "' . $content . '"
                            },';

        }
        return substr($global_vars, 0, -1);
    }
}
